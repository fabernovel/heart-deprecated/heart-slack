## [1.0.5] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

## [1.0.4] - 2019-06-26
### Changed
- Improve README
- Improve .env.sample
- Change wording for Slack notification message

## [1.0.3] - 2019-06-17
### Security
- Fixed NPM dependeny warning 
- Updated every NPM dependencies

## [1.0.2] - 2019-04-18
### Added
- NPM publish as GitLab CD
- `@fabernovel/heart-server`is now a peerDependency

### Changed
- Upgrade `@fabernovel/heart-core`to latest version (the one that embed module loading code)
- Updated code according to the upgrade
- Updated the readme and contributing guide
- Improved the GitLab CI/CD

### Fixed
- NPM permissions from the GitLab CI/CD

## [1.0.0] - 2019-04-10
### Added
- First release (Yay!)
